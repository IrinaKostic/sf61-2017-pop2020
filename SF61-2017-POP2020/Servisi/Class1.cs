﻿using SF61_2017_POP2020.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF61_2017_POP2020.Servisi
{
    class Class1
    {

        DomZdravlja NadjiIdDz(int id)
        {

            foreach (DomZdravlja adresa in Util.Instance.Domovi)
            {
                if (adresa.ID.Equals(id))
                {
                    return adresa;
                }
            }
            return null;
        }



        Termin NadjiIdTermin(int id)
        {

            foreach (Termin termin in Util.Instance.Termini)
            {
                if (termin.ID.Equals(id))
                {
                    return termin;
                }
            }
            return null;
        }

        Adresa NadjiIdAdresa(int id)
        {

            foreach (Adresa adresa in Util.Instance.Adrese)
            {
                if (adresa.ID.Equals(id))
                {
                    return adresa;
                }
            }
            return null;
        }

        Terapija NadjiIdTerapija(int id)
        {

            foreach (Terapija terapija in Util.Instance.Terapije)
            {
                if (terapija.ID.Equals(id))
                {
                    return terapija;
                }
            }
            return null;
        }


        Lekar NadjiIdLekar(int id)
        {

            foreach (Lekar lekar in Util.Instance.Lekari)
            {
                if (lekar.ID.Equals(id))
                {
                    return lekar;
                }
            }
            return null;
        }


        Pacijent NadjiIdPacijent(int id)
        {

            foreach (Pacijent pacijent in Util.Instance.Pacijenti)
            {
                if (pacijent.ID.Equals(id))
                {
                    return pacijent;
                }
            }
            return null;
        }














    }
}
