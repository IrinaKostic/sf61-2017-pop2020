﻿using SF61_2017_POP2020.Model;
using SF61_2017_POP2020.MyExceptions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF61_2017_POP2020.Servisi
{
    public class KorisnikService : IKorisnik
    {
        public void brisanjeKorisnika(string username)
        {                                    //lista korisnika
            Korisnik k = Util.Instance.Korisnici.ToList().Find(korisnik => korisnik.KorisnickoIme.Equals(username));

            if (k == null)
                throw new UserNotFoundException($"Ne postoji korisnik sa korisnickim imenom {username}");
            k.Aktivan = false;

            azurirajKorisnika(k);//update user
        }

       

        public void citanjeKorisnika()
        {
            Util.Instance.Korisnici = new ObservableCollection<Korisnik>();//svaki put se prvo cita iz liste korisnici
            Util.Instance.Adrese = new ObservableCollection<Adresa>();

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select k.*, a.* from korisnici k left join adresa a on k.ID_Adresa = a.ID";
             

                SqlDataReader reader = command.ExecuteReader();//cita korisnike

                

                while (reader.Read())
                {
                   
                    var korisnik = new Korisnik
                    {
                        ID = reader.GetInt32(0),
                        KorisnickoIme = reader.GetString(1),
                        Ime = reader.GetString(2),
                        Prezime = reader.GetString(3),
                        JMBG = reader.GetString(4),
                        Email = reader.GetString(5),
                        Lozinka = reader.GetString(6),
                       // Adresa = adresa(7),
                        Pol = (EPol)reader.GetInt32(8),
                        TipKorisnika = (ETipKorisnika)reader.GetInt32(9),
                        Aktivan = reader.GetBoolean(10)
                    };

                    var adresa = new Adresa()
                    {
                        ID = reader.GetInt32(11),
                        Ulica = reader.GetString(12),
                        Broj = reader.GetInt32(13),
                        Grad = reader.GetString(14),
                        Drzava = reader.GetString(15)
                    };

                    korisnik.Adresa = adresa;
                    Util.Instance.Korisnici.Add(korisnik);

                    Util.Instance.Adrese.Add(adresa);
                }

                reader.Close();

            }
        }

        public int cuvanjeKorisnika(Object obj)
        {
            Korisnik korisnik = obj as Korisnik;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))//konekcija
            {
                conn.Open();//otvara se konekcija

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"insert into dbo.Korisnici (KorisnickoIme, Ime, Prezime, JMBG, Email, Lozinka, ID_Adresa, Pol, TipKorisnika, Aktivan)
                                        output inserted.id VALUES (@KorisnickoIme, @Ime, @Prezime, @JMBG, @Email, @Lozinka, @ID_Adresa, @Pol, @TipKorisnika, @Aktivan)";
                //radi se insert kolona tabele
                //vraca id bas tog korisnika
                command.Parameters.Add(new SqlParameter("KorisnickoIme", korisnik.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("Ime", korisnik.Ime));
                command.Parameters.Add(new SqlParameter("Prezime", korisnik.Prezime));
                command.Parameters.Add(new SqlParameter("JMBG", korisnik.JMBG));
                command.Parameters.Add(new SqlParameter("Email", korisnik.Email));
                command.Parameters.Add(new SqlParameter("Lozinka", korisnik.Lozinka));
                command.Parameters.Add(new SqlParameter("Adresa", korisnik.Adresa.ID));
                command.Parameters.Add(new SqlParameter("Pol", korisnik.Pol.ToString()));
                command.Parameters.Add(new SqlParameter("TipKorisnika", korisnik.TipKorisnika.ToString()));
                command.Parameters.Add(new SqlParameter("Aktivan", korisnik.Aktivan));

                return (int)command.ExecuteScalar();//vraca vrednost id-a usera
            }
            
        }

        public void azurirajKorisnika(Object obj)
        {
           Korisnik korisnik = obj as Korisnik;
            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"update dbo.korisnici
                                        SET Aktivan = @Aktivan
                                        where KorisnickoIme = @KorisnickoIme, Ime = @Ime, Prezime = @Prezime, JMBG= @JMBG, Email = @Email, Lozinka = @Lozinka, ID_Adresa = @ID_Adresa, Pol = @Pol, TipKorisnika = @TipKorisnika Aktivan=@Aktivan";


                command.Parameters.Add(new SqlParameter("KorisnickoIme", korisnik.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("Ime", korisnik.Ime));
                command.Parameters.Add(new SqlParameter("Prezime", korisnik.Prezime));
                command.Parameters.Add(new SqlParameter("JMBG", korisnik.JMBG));
                command.Parameters.Add(new SqlParameter("Email", korisnik.Email));
                command.Parameters.Add(new SqlParameter("Lozinka", korisnik.Lozinka));
                command.Parameters.Add(new SqlParameter("ID_Adresa", korisnik.Adresa));
                command.Parameters.Add(new SqlParameter("Pol", korisnik.Pol.ToString()));
                command.Parameters.Add(new SqlParameter("TipKorisnika", korisnik.TipKorisnika.ToString()));
                command.Parameters.Add(new SqlParameter("Aktivan", korisnik.Aktivan));

                command.ExecuteNonQuery();
            }
        }
    }
 }
