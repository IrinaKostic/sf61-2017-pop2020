﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF61_2017_POP2020.Servisi
{
     public interface IEntitet
    {
        void citanjeEntiteta();
        int cuvanjeEntiteta(Object obj);
        void azurirajEntiteta(Object obj);
        void brisanjeEntiteta(int id);
        
    }
}
