﻿using SF61_2017_POP2020.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF61_2017_POP2020.Servisi
{
    public class AdresaService : IEntitet
    {
       

        public void azurirajEntiteta(object obj)
        {
            throw new NotImplementedException();
        }

        public void brisanjeEntiteta(int id)
        {
            throw new NotImplementedException();
        }

        public void citanjeEntiteta()
        {
            Util.Instance.Adrese = new ObservableCollection<Adresa>();
            
            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from adresa";


                SqlDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {
                    Util.Instance.Adrese.Add(new Adresa
                    {
                        ID = reader.GetInt32(0),
                        Ulica = reader.GetString(1),
                        Broj= reader.GetInt32(2),
                        Grad = reader.GetString(3),
                        Drzava = reader.GetString(4),
                       
                    });
                   

                }

                reader.Close();
            }
        }

        public int cuvanjeEntiteta(object obj)
        {
            Adresa adresa = obj as Adresa;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();//komanda
                command.CommandText = @"insert into dbo.Adresa (ID,Ulica,Broj,Grad,Drzava)
                                        output inserted.id VALUES (@ID,@Ulica,@Broj,@Grad,@Drzava)";
                //vrati vrednost id
                command.Parameters.Add(new SqlParameter("ID", adresa.ID));
                command.Parameters.Add(new SqlParameter("Ulica", adresa.Ulica));
                command.Parameters.Add(new SqlParameter("Broj", adresa.Broj));
                command.Parameters.Add(new SqlParameter("Grad", adresa.Grad));
                command.Parameters.Add(new SqlParameter("Drzava", adresa.Drzava));

                return (int)command.ExecuteScalar();//vrati vrednost id
            }
        }
    }
}
