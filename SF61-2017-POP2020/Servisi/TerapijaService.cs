﻿using SF61_2017_POP2020.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF61_2017_POP2020.Servisi
{
    public class TerapijaService : IEntitet
    {
        public void azurirajEntiteta(object obj)
        {
            throw new NotImplementedException();
        }

        public void brisanjeEntiteta(int id)
        {
            throw new NotImplementedException();
        }

        public void citanjeEntiteta()
        {
            Util.Instance.Terapije= new ObservableCollection<Terapija>();
           Util.Instance.Lekari = new ObservableCollection<Lekar>();
         



            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select t.*, l.* from terapija t left join lekar l on t.ID_Lekar = l.ID";

                SqlDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {
                    Lekar NadjiIdLekar(int id)
                    {

                        foreach (Lekar lekarr in Util.Instance.Lekari)
                        {
                            if (lekarr.ID.Equals(id))
                            {
                                return lekarr;
                            }
                        }
                        return null;
                    }

                    DomZdravlja NadjiIdDz(int id)
                    {

                        foreach (DomZdravlja dz in Util.Instance.Domovi)
                        {
                            if (dz.ID.Equals(id))
                            {
                                return dz;
                            }
                        }
                        return null;
                    }
                    Adresa NadjiIdAdresa(int id)
                    {

                        foreach (Adresa adresaa in Util.Instance.Adrese)
                        {
                            if (adresaa.ID.Equals(id))
                            {
                                return adresaa;
                            }
                        }
                        return null;
                    }

                    Termin NadjiIdTermin(int id)
                    {

                        foreach (Termin termin in Util.Instance.Termini)
                        {
                            if (termin.ID.Equals(id))
                            {
                                return termin;
                            }
                        }
                        return null;
                    }
                    var terapija = new Terapija()
                    {
                        ID = reader.GetInt32(0),
                        Opis = reader.GetString(1),
                        Lekar = NadjiIdLekar(2)


                    };


                    var lekar = new Lekar()
                    {
                        ID = reader.GetInt32(0),
                        DomZdravlja = NadjiIdDz(1),
                        Termin = NadjiIdTermin(2),

                    };

                    terapija.Lekar = lekar;
                   
                    Util.Instance.Terapije.Add(terapija);
                    Util.Instance.Lekari.Add(lekar);

                }

                reader.Close();
            }
        }

        public int cuvanjeEntiteta(object obj)
        {
            Terapija terapija = obj as Terapija;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();//komanda
                command.CommandText = @"insert into dbo.Terapija (ID,,Opis,ID_Lekar)
                                        output inserted.id VALUES (@ID,@Opis,@ID_Lekar)";
                //vrati vrednost id
                command.Parameters.Add(new SqlParameter("ID", terapija.ID));
                command.Parameters.Add(new SqlParameter("Opis", terapija.Opis));
                command.Parameters.Add(new SqlParameter("ID_Lekar", terapija.Lekar.ID));
               

                return (int)command.ExecuteScalar();//vrati vrednost id
            }
        }
    }
}
