﻿using SF61_2017_POP2020.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF61_2017_POP2020.Servisi
{
    public class TerminService : IEntitet
    {
        public void azurirajEntiteta(object obj)
        {
            throw new NotImplementedException();
        }

        public void brisanjeEntiteta(int id)
        {
            throw new NotImplementedException();
        }

        public void citanjeEntiteta()
        {
            Util.Instance.Termini = new ObservableCollection<Termin>();
            // Util.Instance.Domovi = new ObservableCollection<DomZdravlja>(); mozda posto ga lekar ima
            Util.Instance.Lekari = new ObservableCollection<Lekar>();
            Util.Instance.Pacijenti = new ObservableCollection<Pacijent>();



            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select t.*, l.* from termin l left join lekar l on t.ID_Lekar = l.ID";
                command.CommandText = @"sepect t.*, p.* from termin p peft join pacijent p on t.ID_Pacijent = p.ID";

                command.CommandText = @"select * from termin ";
                

                SqlDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {
                    Lekar NadjiIdLekar(int id)
                    {

                        foreach (Lekar lekarr in Util.Instance.Lekari)
                        {
                            if (lekarr.ID.Equals(id))
                            {
                                return lekarr;
                            }
                        }
                        return null;
                    }


                    Pacijent NadjiIdPacijent(int id)
                    {

                        foreach (Pacijent pacijentt in Util.Instance.Pacijenti)
                        {
                            if (pacijentt.ID.Equals(id))
                            {
                                return pacijentt;
                            }
                        }
                        return null;
                    }

                    DomZdravlja NadjiIdDz(int id)
                    {

                        foreach (DomZdravlja dz in Util.Instance.Domovi)
                        {
                            if (dz.ID.Equals(id))
                            {
                                return dz;
                            }
                        }
                        return null;
                    }

                    Termin NadjiIdTermin(int id)
                    {

                        foreach (Termin terminn in Util.Instance.Termini)
                        {
                            if (terminn.ID.Equals(id))
                            {
                                return terminn;
                            }
                        }
                        return null;
                    }



                    Terapija NadjiIdTerapija(int id)
                    {

                        foreach (Terapija terapijaa in Util.Instance.Terapije)
                        {
                            if (terapijaa.ID.Equals(id))
                            {
                                return terapijaa;
                            }
                        }
                        return null;
                    }




                    var termin = new Termin()
                    {
                        ID = reader.GetInt32(0),
                        Lekar= NadjiIdLekar(1),
                        Datum = reader.GetString(2),
                        StatusTermina = (EStatusTermina)reader.GetInt32(3),//3
                        pacijent = NadjiIdPacijent(4)


                    };


                   var lekar = new Lekar()
                    {
                        ID = reader.GetInt32(0),
                        DomZdravlja = NadjiIdDz(1),
                        Termin = NadjiIdTermin(2),

                    };

                    var pacijent = new Pacijent()
                    {
                        ID = reader.GetInt32(0),
                        Termin = NadjiIdTermin(1),
                        Terapija= NadjiIdTerapija(2)
                       

                    };
                    Util.Instance.Lekari.Add(lekar);
                    Util.Instance.Pacijenti.Add(pacijent);
                    Util.Instance.Termini.Add(termin);
                    termin.Lekar = lekar;
                    termin.pacijent = pacijent;
                }

                reader.Close();
            }
        }

        public int cuvanjeEntiteta(object obj)
        {
            Termin termin = obj as Termin;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();//komanda
                command.CommandText = @"insert into dbo.Termin (ID,ID_Lekar,Datum.,StatusTermina,ID_Pacijent)
                                        output inserted.id VALUES (@ID,@ID_Lekar,@Datum,@StatusTermina,@ID_Pacijent)";
                //vrati vrednost id
                command.Parameters.Add(new SqlParameter("ID", termin.ID));
                command.Parameters.Add(new SqlParameter("ID_Lekar", termin.Lekar.ID));
                command.Parameters.Add(new SqlParameter("Datum", termin.Datum));
                command.Parameters.Add(new SqlParameter("StatusTermina", termin.StatusTermina.ToString()));
                command.Parameters.Add(new SqlParameter("ID_Pacijenta", termin.pacijent.ID));


                return (int)command.ExecuteScalar();//vrati vrednost id
            }
        }
    }
}
