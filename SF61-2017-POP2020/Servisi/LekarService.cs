﻿using SF61_2017_POP2020.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF61_2017_POP2020.Servisi
{
    
        public class LekarService : IKorisnik
        {
        public void brisanjeKorisnika(string username)
        {
            
        }


        public void citanjeKorisnika()
        {
            Util.Instance.Lekari = new ObservableCollection<Lekar>();
            Util.Instance.Korisnici = new ObservableCollection<Korisnik>();

            Util.Instance.Adrese = new ObservableCollection<Adresa>();
            Util.Instance.Domovi = new ObservableCollection<DomZdravlja>();
            Util.Instance.Termini = new ObservableCollection<Termin>();
            Util.Instance.Pacijenti = new ObservableCollection<Pacijent>();

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select k.*, a.* from korisnici k left join adresa a on k.ID_Adresa = a.ID";
               // command.CommandText = @"select l.*, d.* from lekar l left join domzdravlja d on l.ID_DomZdravlja = d.ID";
               
                //command.CommandText = @"select l.*, t.* from lekar l left join termin t on l.ID_Termin = t.ID";

                SqlDataReader reader = command.ExecuteReader();//cita korisnike
                

                while (reader.Read())
                {
                    /*DomZdravlja NadjiIdDz(int id)
                    {

                        foreach (DomZdravlja dz in Util.Instance.Domovi)
                        {
                            if (dz.ID.Equals(id))
                            {
                                return dz;
                            }
                        }
                        return null;
                    }



                    Termin NadjiIdTermin(int id)
                    {

                        foreach (Termin termin in Util.Instance.Termini)
                        {
                            if (termin.ID.Equals(id))
                            {
                                return termin;
                            }
                        }
                        return null;
                    }



                    Korisnik NadjiIdKorisnik(int id)
                    {

                        foreach (Korisnik k in Util.Instance.Korisnici)
                        {
                            if (k.ID.Equals(id))
                            {
                                return k;
                            }
                        }
                        return null;
                    }*/

                    var korisnik = new Korisnik
                    {
                        ID = reader.GetInt32(0),
                        KorisnickoIme = reader.GetString(1),
                        Ime = reader.GetString(2),
                        Prezime = reader.GetString(3),
                        JMBG = reader.GetString(4),
                        Email = reader.GetString(5),
                        Lozinka = reader.GetString(6),
                        // Adresa = adresa(7),
                        Pol = (EPol)reader.GetInt32(8),
                        TipKorisnika = (ETipKorisnika)reader.GetInt32(9),//9
                        Aktivan = reader.GetBoolean(10)
                    };

                    var adresa = new Adresa()
                    {
                        ID = reader.GetInt32(11),
                        Ulica = reader.GetString(12),
                        Broj = reader.GetInt32(13),
                        Grad = reader.GetString(14),
                        Drzava = reader.GetString(15)
                    };

                    /*var lekar = new Lekar() 
                    { 
                     ID = reader.GetInt32(16),
                     //Korisnicko =NadjiIdKorisnik(17),
                     //DomZdravlja = NadjiIdDz(18),
                     //Termin =NadjiIdTermin(19)
                    };*/

                    korisnik.Adresa = adresa;
                    Util.Instance.Korisnici.Add(korisnik);
                    Util.Instance.Adrese.Add(adresa);


                }

                reader.Close();
            }
            }

        public int cuvanjeKorisnika(object obj)
        {
            Lekar lekar = obj as Lekar;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();//komanda
                command.CommandText = @"insert into dbo.Lekar (ID,ID_DomZdravlja,ID_Termin)
                                        output inserted.id VALUES (@ID,@ID_DomZdravlja,@ID_Termin)";
                //vrati vrednost id
                command.Parameters.Add(new SqlParameter("ID", lekar.ID));
                command.Parameters.Add(new SqlParameter("ID_DomZdravlja", lekar.DomZdravlja.ID));
                command.Parameters.Add(new SqlParameter("ID_Termin", lekar.Termin.ID));

                return (int)command.ExecuteScalar();//vrati vrednost id
            }
        }

        public void azurirajKorisnika(object obj)
        {
            throw new NotImplementedException();
        }

      
    }
    }