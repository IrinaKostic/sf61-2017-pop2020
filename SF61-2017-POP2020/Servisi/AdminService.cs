﻿using SF61_2017_POP2020.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF61_2017_POP2020.Servisi
{
    public class AdminService : IKorisnik
    {
      
        public void citanjeKorisnika()
        {
            Util.Instance.Admini = new ObservableCollection<Admin>();
            Util.Instance.Korisnici = new ObservableCollection<Korisnik>();

            Util.Instance.Adrese = new ObservableCollection<Adresa>();
            Util.Instance.Domovi = new ObservableCollection<DomZdravlja>();
            Util.Instance.Termini = new ObservableCollection<Termin>();
            Util.Instance.Terapije = new ObservableCollection<Terapija>();

            //Util.Instance.Pacijenti = new ObservableCollection<Pacijent>();//posto u erminu ima pacijenta

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select k.*, a.* from korisnici k left join adresa a on k.ID_Adresa = a.ID";

                SqlDataReader reader = command.ExecuteReader();//cita korisnike


                while (reader.Read())
                {

                    var korisnik = new Korisnik
                    {
                        ID = reader.GetInt32(0),
                        KorisnickoIme = reader.GetString(1),
                        Ime = reader.GetString(2),
                        Prezime = reader.GetString(3),
                        JMBG = reader.GetString(4),
                        Email = reader.GetString(5),
                        Lozinka = reader.GetString(6),
                        // Adresa = adresa(7),
                        Pol = (EPol)reader.GetInt32(8),
                        TipKorisnika = (ETipKorisnika)reader.GetInt32(9),
                        Aktivan = reader.GetBoolean(10)
                    };

                    var adresa = new Adresa()
                    {
                        ID = reader.GetInt32(11),
                        Ulica = reader.GetString(12),
                        Broj = reader.GetInt32(13),
                        Grad = reader.GetString(14),
                        Drzava = reader.GetString(15)
                    };

                    korisnik.Adresa = adresa;
                    Util.Instance.Korisnici.Add(korisnik);
                    Util.Instance.Adrese.Add(adresa);


                }

                reader.Close();
            }
        }

        public int cuvanjeKorisnika(object obj)
        {
            Admin admin = obj as Admin;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();//komanda
                command.CommandText = @"insert into dbo.Admin (ID,ID_DomZdravlja,ID_Termin,ID_Terapija)
                                        output inserted.id VALUES (@ID,@ID_DomZdravlja,@ID_Termin,@ID_Terapija)";
                //vrati vrednost id
                command.Parameters.Add(new SqlParameter("ID", admin.ID));
                command.Parameters.Add(new SqlParameter("ID_DomZdravlja", admin.DomZdravlja.ID));
                command.Parameters.Add(new SqlParameter("ID_Termin", admin.Termin.ID));
                command.Parameters.Add(new SqlParameter("ID_Terapija", admin.Terapija.ID));

                return (int)command.ExecuteScalar();//vrati vrednost id
            }
        }

        public void azurirajKorisnika(object obj)
        {
            throw new NotImplementedException();
        }

        public void brisanjeKorisnika(string username)
        {
            throw new NotImplementedException();
        }
    }
}
