﻿using SF61_2017_POP2020.Model;
using System;
using System.Collections.ObjectModel;
using System.Data.SqlClient;

namespace SF61_2017_POP2020.Servisi
{
    public class DZService : IEntitet
    {
        public void azurirajEntiteta(object obj)
        {
            throw new NotImplementedException();
        }

        public void brisanjeEntiteta(int id)
        {
            throw new NotImplementedException();
        }

       

        public void citanjeEntiteta()
        {
            Util.Instance.Adrese = new ObservableCollection<Adresa>();
            Util.Instance.Domovi = new ObservableCollection<DomZdravlja>();


            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select d.*, a.* from domzdravlja d left join adresa a on d.ID_Adresa = a.ID";



                SqlDataReader reader = command.ExecuteReader();

              
                while (reader.Read())
                {
                     Adresa NadjiIdAdresa(int id)
                    {

                        foreach (Adresa adresaa in Util.Instance.Adrese)
                        {
                            if (adresaa.ID.Equals(id))
                            {
                                return adresaa;
                            }
                        }
                        return null;
                    }

                    var dom = new DomZdravlja()
                    {
                        ID = reader.GetInt32(0),
                        Naziv = reader.GetString(1),
                        Adresa =NadjiIdAdresa(2)
                 };


                    var adresa = new Adresa()
                    {
                        ID = reader.GetInt32(3),
                        Ulica = reader.GetString(4),
                        Broj = reader.GetInt32(5),
                        Grad = reader.GetString(6),
                        Drzava = reader.GetString(7)
                    };

                    dom.Adresa = adresa;
                    Util.Instance.Domovi.Add(dom);
                    Util.Instance.Adrese.Add(adresa);


                }

                reader.Close();
            }
        }

        public int cuvanjeEntiteta(object obj)
        {
            DomZdravlja domZdravlja = obj as DomZdravlja;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();//komanda
                command.CommandText = @"insert into dbo.DomZdravlja (ID,Naziv,ID_Adresa)
                                        output inserted.id VALUES (@ID,@Naziv,@ID_Adresa)";
                //vrati vrednost id
                command.Parameters.Add(new SqlParameter("ID", domZdravlja.ID));
                command.Parameters.Add(new SqlParameter("Naziv", domZdravlja.Naziv));
                command.Parameters.Add(new SqlParameter("ID_Adresa", domZdravlja.Adresa.ID));
              

                return (int)command.ExecuteScalar();//vrati vrednost id
            }
        }
    }
}
