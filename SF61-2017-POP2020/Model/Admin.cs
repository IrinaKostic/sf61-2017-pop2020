﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF61_2017_POP2020.Model
{
     public class Admin
    {

        private int _id;

        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private Korisnik _korisnicko;

        public Korisnik Korisnicko
        {
            get { return _korisnicko; }
            set { _korisnicko = value; }
        }



        private DomZdravlja _domZdravlja;

        public DomZdravlja DomZdravlja
        {
            get { return _domZdravlja; }
            set { _domZdravlja = value; }
        }

        private Termin _termin;

        public Termin Termin
        {
            get { return _termin; }
            set { _termin = value; }
        }

        private Terapija _terapija;

        public Terapija Terapija
        {
            get { return _terapija; }
            set { _terapija = value; }
        }

    }
}
