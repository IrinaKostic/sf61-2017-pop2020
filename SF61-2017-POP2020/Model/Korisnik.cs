﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF61_2017_POP2020.Model
{
    [Serializable]
    public class Korisnik : IDataErrorInfo
    {
        private int _id;

        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _korisnickoIme;

        public string KorisnickoIme
        {
            get { return _korisnickoIme; }
            set
            {
                if (value != null)
                {
                    if (Util.Instance.Korisnici.ToList().Exists(k => k.KorisnickoIme.Equals(value)))
                    {
                        throw new ArgumentException("Username must be unique!");
                    }
                }
                _korisnickoIme = value;
            }
        }

        private string _ime;

        public string Ime
        {
            get { return _ime; }
            set { _ime = value; }
        }

        private string _prezime;

        public string Prezime
        {
            get { return _prezime; }
            set { _prezime = value; }
        }

        private string _jmbg;

        public string JMBG
        {
            get { return _jmbg; }
            set { _jmbg = value; }
        }

        private string _email;

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        private string _lozinka;

        public string Lozinka
        {
            get { return _lozinka; }
            set { _lozinka = value; }
        }

        
        private Adresa _adresa;

        public Adresa Adresa
        {
            get { return _adresa; }
            set { _adresa = value; }
        }

        private EPol _pol;

        public EPol Pol
        {
            get { return _pol; }
            set { _pol = value; }
        }

        private ETipKorisnika _tipKorisnika;

        public ETipKorisnika TipKorisnika
        {
            get { return _tipKorisnika; }
            set { _tipKorisnika = value; }
        }

        private bool _aktivan;

        public bool Aktivan
        {
            get { return _aktivan; }
            set { _aktivan = value; }
        }

        public string Error
        {
            get
            {
                return "message!";
            }
        }

        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Ime":
                        if (string.IsNullOrEmpty(Ime))//(Ime != null && Ime.Equals(String.Empty))
                            return "Property Ime is manditory!";
                        break;
                    case "Prezime":
                        if (string.IsNullOrEmpty(Prezime))//(Ime != null && Ime.Equals(String.Empty))
                            return "Property Prezime is manditory!";
                        break;


                }
                return String.Empty;
            }
        }


        public Korisnik()
        {

        }

       /* public override string ToString()
        {
            return "Ja sam " + KorisnickoIme + ". Moje email je:" + Email + ". Tip: " + TipKorisnika + ". Moja adresa je " + Adresa.ToString();
        }*/

        public string KorisnikZaUpisUFajl()
        {
            return KorisnickoIme + ";" + Ime + ";" + Prezime + ";" + JMBG + ";" + Adresa + ";" +
                Email + ";" + Lozinka + ";" + Pol + ";" + TipKorisnika + ";" + Aktivan;//cuvanje korisnika po redu iz txt fajla
        }

        public Korisnik Clone()//korisnik koji ima sva polja kao onaj stari
        {
            Korisnik kopija = new Korisnik();

            kopija.Adresa = Adresa;
            kopija.Aktivan = Aktivan;
            kopija.Email = Email;
            kopija.Ime = Ime;
            kopija.Prezime = Prezime;
            kopija.Pol = Pol;
            kopija.Lozinka = Lozinka;
            kopija.JMBG = JMBG;
            kopija.KorisnickoIme = KorisnickoIme;

            return kopija;
        }

    }
}
