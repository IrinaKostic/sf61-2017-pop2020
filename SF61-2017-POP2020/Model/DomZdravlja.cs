﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF61_2017_POP2020.Model
{   [Serializable]
     public class DomZdravlja
    {
        private int _id;

        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }


        private string _naziv;

        public string Naziv
        {
            get { return _naziv; }
            set { _naziv = value; }
        }


        private Adresa _adresa;

        public Adresa Adresa
        {
            get { return _adresa; }
            set { _adresa = value; }
        }

        public override string ToString()
        {
            return "Sifra " + ID + "naziv " + Naziv + "Adresa " + Adresa ;
        }

        public string DZUpisUFajl()
        {
            return ID + ";" + Naziv + ";" + Adresa;
        }
    }
}
