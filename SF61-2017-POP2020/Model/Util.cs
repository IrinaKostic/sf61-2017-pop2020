﻿using SF61_2017_POP2020.Servisi;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF61_2017_POP2020.Model
{
    public sealed class Util
    {
        public static string CONNECTION_STRING = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        private static readonly Util instance = new Util();
        IKorisnik _korisnikService;
        IKorisnik _lekarService;
        IKorisnik _adminService;
        IKorisnik _pacijentService;

        IEntitet _adresaService;
        IEntitet _dzService;
        IEntitet _terminService;
        IEntitet _terapijaService;
       
        private Util()
        {
            _korisnikService = new KorisnikService();
            _lekarService = new LekarService();
            _adminService = new AdminService();
            _pacijentService = new PacijentService();

            _adresaService = new AdresaService();
            _dzService = new DZService();
            _terminService = new TerminService();
            _terapijaService = new TerapijaService();

        }
        static Util()
        {

        }

        public static Util Instance
        {
            get
            {
                return instance;
            }
        }

        public ObservableCollection<Korisnik> Korisnici { get; set; }//lista
        public ObservableCollection<Lekar> Lekari { get; set; }
        public ObservableCollection<Pacijent> Pacijenti { get; set; }
        public ObservableCollection<Admin> Admini { get; set; }


        public ObservableCollection<Adresa> Adrese { get; set; }
        public ObservableCollection<DomZdravlja> Domovi { get; set; }
        public ObservableCollection<Termin> Termini { get; set; }
        public ObservableCollection<Terapija> Terapije { get; set; }

        public void Initialize()
        {
            Korisnici = new ObservableCollection<Korisnik>();//inicijalizacija te liste
            Lekari = new ObservableCollection<Lekar>();//zato szo nasledjuje klasu <korisnik>
            Pacijenti = new ObservableCollection<Pacijent>();
            Admini = new ObservableCollection<Admin>();


            Adrese = new ObservableCollection<Adresa>();
            Domovi = new ObservableCollection<DomZdravlja>();
            Termini = new ObservableCollection<Termin>();
            Terapije = new ObservableCollection<Terapija>();


           /* Adresa adresa = new Adresa//fiksne vrednosti
            {
                Grad = "Grad 1",
                Broj = "Broj1",
                Drzava = "Drzava 1",
                Ulica = "Ulica 1",
                ID = "1"
            };

            Korisnik korisnik1 = new Korisnik();//fiksne vrednosti
            korisnik1.KorisnickoIme = "pera";
            korisnik1.Ime = "petar";
            korisnik1.Prezime = "peric";
            korisnik1.JMBG = "123456";
            korisnik1.Lozinka = "pera";
            korisnik1.Email = "pera@gmail.com";
            korisnik1.Pol = EPol.M;
            korisnik1.TipKorisnika = ETipKorisnika.ADMINISTRATOR;
            korisnik1.Aktivan = true;
            //korisnik1.Adresa = adresa;

            Korisnik korisnik2 = new Korisnik//fiksne vrednosti
            {
                Email = "zika@gmail.com",
                Ime = "zika",
                Prezime = "zikic",
                KorisnickoIme = "ziza",
                JMBG = "654321",
                Lozinka = "zika",
                Pol = EPol.Z,
                TipKorisnika = ETipKorisnika.LEKAR,
                //Adresa = adresa
            };

            Lekar lekar = new Lekar
            {
                DomZdravlja = "Dom Zdravlja 1",
                Korisnicko = korisnik2
            };

            Korisnici.Add(korisnik1);
            Korisnici.Add(korisnik2);

            Lekari = new ObservableCollection<Lekar>
            {
                lekar
            };*/

        }

        public int SacuvajK(Object obj)//cuva entitet prema objektu
        {
            if (obj is Korisnik)
            {
                return _korisnikService.cuvanjeKorisnika(obj);
            }
            else if (obj is Lekar)
            {
                return _lekarService.cuvanjeKorisnika(obj);
            }
            else if (obj is Admin)
            {
                return _adminService.cuvanjeKorisnika(obj);
            }
            else if (obj is Pacijent)
            {
                return _pacijentService.cuvanjeKorisnika(obj);
            }

            return -1;
        }


        public int SacuvajE(Object obj)
        {
            if (obj is Adresa)
            {
                return _adresaService.cuvanjeEntiteta(obj);
            }
            else if (obj is DomZdravlja)
            {
                return _dzService.cuvanjeEntiteta(obj);
            }
            else if (obj is Termin)
            {
                return _terminService.cuvanjeEntiteta(obj);
            }
            else if (obj is Pacijent)
            {
                return _terapijaService.cuvanjeEntiteta(obj);
            }

            return -1;
        }

        public void CitajK(string filename)
        {
            if (filename.Contains("korisnici"))
            {
                _korisnikService.citanjeKorisnika();
            }
            else if (filename.Contains("lekari"))
            {
                _lekarService.citanjeKorisnika();
            }
            else if (filename.Contains("admini"))
            {
                _adminService.citanjeKorisnika();
            }
            else if (filename.Contains("pacijenti"))
            {
                _pacijentService.citanjeKorisnika();
            }
        }

        public void CitajE(string filename)
        {
            if (filename.Contains("adrese"))
            {
                _adresaService.citanjeEntiteta();
            }
            else if (filename.Contains("dz"))
            {
                _dzService.citanjeEntiteta();
            }
            else if (filename.Contains("termini"))
            {
                _terminService.citanjeEntiteta();
            }
            else if (filename.Contains("terapije"))
            {
                _terapijaService.citanjeEntiteta();
            }
        }
        public void DeleteUser(string username)
        {
            _korisnikService.brisanjeKorisnika(username);
            _lekarService.brisanjeKorisnika(username);
            _adminService.brisanjeKorisnika(username);
            _pacijentService.brisanjeKorisnika(username);
        }

       

    }
}