﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF61_2017_POP2020.Model
{
    public class Termin
    {

        private int _id;

        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }


        private Lekar _lekar;

        public Lekar Lekar
        {
            get { return _lekar; }
            set { _lekar = value; }
        }

        private string _datum;

        public string Datum
        {
            get { return _datum; }
            set { _datum = value; }
        }

        private EStatusTermina _statusTermina;

        public EStatusTermina StatusTermina
        {
            get { return _statusTermina; }
            set { _statusTermina = value; }
        }

        private Pacijent _pacijent;

        public Pacijent pacijent

        {
            get { return _pacijent; }
            set { _pacijent = value; }
        }


    }
}
