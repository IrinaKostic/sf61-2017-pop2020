﻿using SF61_2017_POP2020.Model;
using SF61_2017_POP2020.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF61_2017_POP2020
{
    /// <summary>
    /// Interaction logic for HomeWindow.xaml
    /// </summary>
    public partial class HomeWindow : Window
    {
        public HomeWindow()
        {
            InitializeComponent();
            Util.Instance.CitajK("korisnici");
            Util.Instance.CitajK("lekari");
            Util.Instance.CitajK("admini");
            Util.Instance.CitajK("pacijenti");

            Util.Instance.CitajE("adrese");
            Util.Instance.CitajE("dz");
            Util.Instance.CitajE("termini");
            Util.Instance.CitajE("terapije");


        }

        private void BtnStart_LEKAR(object sender, RoutedEventArgs e)
        {
           AllDoctors window = new AllDoctors();//dugme preko koga se prelazi sa home na doktore

            this.Hide();
            window.Show();
        }

        private void BtnStart_ADMIN(object sender, RoutedEventArgs e)
        {
            SviAdmini window = new SviAdmini();//dugme preko koga se prelazi sa home na ADMINE

            this.Hide();
            window.Show();
        }

        private void BtnStart_PACIJENT(object sender, RoutedEventArgs e)
        {
            SviPacijenti window = new SviPacijenti();//dugme preko koga se prelazi sa home na PACIJENTE

            this.Hide();
            window.Show();
        }
    }
}
