﻿using SF61_2017_POP2020.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF61_2017_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for SviPacijenti.xaml
    /// </summary>
    public partial class SviPacijenti : Window
    {
        ICollectionView view;

        public SviPacijenti()
        {
            InitializeComponent();

            UpdateView();

            view.Filter = CustomFilter;
        }

        private bool CustomFilter(object obj)
        {
            Korisnik korisnik = obj as Korisnik;

            if (korisnik.TipKorisnika.Equals(ETipKorisnika.PACIJENT) && korisnik.Aktivan)
                if (TxtPretraga.Text != "")//ako je txt pretrage razlicit od nicega 
                {
                    return korisnik.Ime.Contains(TxtPretraga.Text);//ako sadrze neko slovo ili slova od imena
                }
                else
                    return true;
            return false;
        }

        private void UpdateView()
        {
            view = CollectionViewSource.GetDefaultView(Util.Instance.Korisnici);
            DGPacijenti.ItemsSource = view; // Util.Instance.Korisnici;
            DGPacijenti.IsSynchronizedWithCurrentItem = true;
            DGPacijenti.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
        }

        private void DGPacijenti_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Aktivan") || e.PropertyName.Equals("Error"))
                e.Column.Visibility = Visibility.Collapsed;
        }

        private void MIDodajPacijenta_Click(object sender, RoutedEventArgs e)
        {
            Korisnik noviKorisnik = new Korisnik();

            //DodajIzmeniPacijenta add = new DodajIzmeniPacijenta(
            DodajIzmeniPacijenta add = new DodajIzmeniPacijenta(noviKorisnik);

            this.Hide();
            if (!(bool)add.ShowDialog())
            {

            }
            this.Show();
            view.Refresh();
        }

        private void MIIzmeniPacijenta_Click(object sender, RoutedEventArgs e)
        {
            //Korisnik selektovan = (Korisnik)DGPacijenti.SelectedItem;
            Korisnik selektovan = view.CurrentItem as Korisnik;
            Korisnik stariPacijent = selektovan.Clone();

            DodajIzmeniPacijenta add = new DodajIzmeniPacijenta(selektovan, EStatus.Izmeni);

            this.Hide();
            if (!(bool)add.ShowDialog())//bool bira izmedju ok i cancel dugmeta u zavisnosti od toga  ce se nestodesiti
            {
                int index = Util.Instance.Korisnici.ToList().FindIndex(k => k.KorisnickoIme.Equals(selektovan.KorisnickoIme));//pronalazi index korisnika
                Util.Instance.Korisnici[index] = stariPacijent;
            }
            this.Show();
            view.Refresh();//kada se uradi dodavanje il izmena dodaje se ovo kako bi radio custom filter
        }

        private void ObrisiPacijentaMI_Click(object sender, RoutedEventArgs e)
        {
            Korisnik selektovan = view.CurrentItem as Korisnik;
            Util.Instance.DeleteUser(selektovan.KorisnickoIme);

            view.Refresh();
            /*int index = Util.Instance.Pacijenti.ToList().FindIndex(u => u.Korisnicko.KorisnickoIme.Equals(obrisiPacijent.KorisnickoIme));
            Util.Instance.Pacijenti[index].Korisnicko.Aktivan = false;*/

            //UpdateView();
        }

        private void TxtPretraga_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void DGPacijenti_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            Korisnik selektovan = view.CurrentItem as Korisnik;
            if (selektovan != null)
            {
                ContextMenu contextMenu = new ContextMenu();
                MenuItem mi = new MenuItem();
                mi.Header = "Edit";
                mi.Click += Edit_Click;
                contextMenu.Items.Add(mi);
                DGPacijenti.ContextMenu = contextMenu;
            }
        }

        void Edit_Click(object sender, RoutedEventArgs e)
        {
            Korisnik selektovan = view.CurrentItem as Korisnik;
            Korisnik stariPacijent = selektovan.Clone();

            DodajIzmeniPacijenta add = new DodajIzmeniPacijenta(selektovan, EStatus.Izmeni);

            //this.Hide();
            if (!(bool)add.ShowDialog())
            {
                int index = Util.Instance.Korisnici.ToList().FindIndex(k => k.KorisnickoIme.Equals(selektovan.KorisnickoIme));
                Util.Instance.Korisnici[index] = stariPacijent;
            }
            //this.Show();
            view.Refresh();
        }
    }
}
