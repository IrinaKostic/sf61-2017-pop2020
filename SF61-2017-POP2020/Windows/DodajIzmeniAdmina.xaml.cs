﻿using SF61_2017_POP2020.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF61_2017_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for DodajIzmeniAdmina.xaml
    /// </summary>
    public partial class DodajIzmeniAdmina : Window
    {
        private EStatus odabranStatus;
        private Korisnik odabranAdmin;

        public DodajIzmeniAdmina(Korisnik admin,EStatus status = EStatus.Dodaj)
        {
            InitializeComponent();

            this.DataContext = admin;

            odabranAdmin = admin;
            odabranStatus = status;

            CmbTipKorisnika.ItemsSource = Enum.GetValues(typeof(ETipKorisnika)).Cast<ETipKorisnika>();//tip koriksnikka veszuje se za cb

            if (status.Equals(EStatus.Izmeni) && admin != null)//ako je status izmeni i admin nije null(znaci da postoji pa moze da se menja)
            {
                this.Title = "Izmeni admina";
               
                
                TxtKorisnickoIme.IsEnabled = false;
            }
            else
            {
                this.Title = "Dodaj admina";
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {

            this.DialogResult = false;
            this.Close();
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            if (!Validation.GetHasError(TxtKorisnickoIme) && !Validation.GetHasError(TxtEmail) && !Validation.GetHasError(TxtName))
            {
                if (odabranStatus.Equals(EStatus.Dodaj))
                {
                    odabranAdmin.Aktivan = true;
                    Admin admin = new Admin
                    {
                    Korisnicko = odabranAdmin
                    /*termin id,
                     * terapija id,
                     * dz id*/
                    };

                    Util.Instance.Korisnici.Add(odabranAdmin);
                    Util.Instance.Admini.Add(admin);
                    ///TODO:mozda ovako da v raca i id terapije npr
                    int id = Util.Instance.SacuvajK(odabranAdmin);//vraca id admina koji je sacuvan u tabeli-korisnikka koji je sacuva u tabeli
                    admin.ID = id;
                    Util.Instance.SacuvajK(admin);
                }

                this.DialogResult = true;
                this.Close();
            }


        }
    }
}
