﻿using SF61_2017_POP2020.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF61_2017_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for DodajIzmeniPacijenta.xaml
    /// </summary>
    public partial class DodajIzmeniPacijenta : Window
    {
        private EStatus odabranStatus;
        private Korisnik odabranPacijent;

        public DodajIzmeniPacijenta(Korisnik pacijent, EStatus status = EStatus.Dodaj)
        {
        
                InitializeComponent();

                this.DataContext = pacijent;

                odabranPacijent = pacijent;
                odabranStatus = status;

                CmbTipKorisnika.ItemsSource = Enum.GetValues(typeof(ETipKorisnika)).Cast<ETipKorisnika>();//tip koriksnikka veszuje se za cb

                if (status.Equals(EStatus.Izmeni) && pacijent != null)//ako je status izmeni i pacijent nije null(znaci da postoji pa moze da se menja)
                {
                    this.Title = "Izmeni pacijenta";


                    TxtKorisnickoIme.IsEnabled = false;
                }
                else
                {
                    this.Title = "Dodaj pacijenta";
                }
            }

            private void BtnCancel_Click(object sender, RoutedEventArgs e)
            {

                this.DialogResult = false;
                this.Close();
            }

            private void BtnOk_Click(object sender, RoutedEventArgs e)
            {
                if (!Validation.GetHasError(TxtKorisnickoIme) && !Validation.GetHasError(TxtEmail) && !Validation.GetHasError(TxtName))
                {
                    if (odabranStatus.Equals(EStatus.Dodaj))
                    {
                        odabranPacijent.Aktivan = true;
                        Pacijent pacijent = new Pacijent
                        {
                            Korisnicko = odabranPacijent
                            /*termin id,
                             * terapija id,
                             *  id*/
                        };

                        Util.Instance.Korisnici.Add(odabranPacijent);
                        Util.Instance.Pacijenti.Add(pacijent);
                        ///TODO:mozda ovako da v raca i id terapije npr
                        int id = Util.Instance.SacuvajK(odabranPacijent);//vraca id pacijenta koji je sacuvan u tabeli-korisnikka koji je sacuva u tabeli
                        pacijent.ID = id;
                        Util.Instance.SacuvajK(pacijent);
                    }

                    this.DialogResult = true;
                    this.Close();
                }


            }
        }
    }

