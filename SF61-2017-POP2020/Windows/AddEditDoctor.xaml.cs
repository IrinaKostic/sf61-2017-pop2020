﻿using SF61_2017_POP2020.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF61_2017_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for AddEditDoctor.xaml
    /// </summary>
    public partial class AddEditDoctor : Window
    {
        private EStatus odabranStatus;
        private Korisnik odabranLekar;
        private DomZdravlja dzL;
        public AddEditDoctor(Korisnik lekar, EStatus status = EStatus.Dodaj)//ako nije izmena podrazumevano je dodaj a jorisnik da je lekar
        {
            InitializeComponent();

            this.DataContext = lekar;

            odabranLekar = lekar;
            odabranStatus = status;

            CmbTipKorisnika.ItemsSource = Enum.GetValues(typeof(ETipKorisnika)).Cast<ETipKorisnika>();//tip koriksnikka veszuje se za cb

            if (status.Equals(EStatus.Izmeni) && lekar != null)//ako je status izmeni i lekar nije null(znaci da postoji pa moze da se menja)
            {
                this.Title = "Izmeni lekara";
                /*TxtEmail.Text = lekar.Email;
                TxtKorisnickoIme.Text = lekar.KorisnickoIme;
                TxtName.Text = lekar.Ime;
                TxtPrezime.Text = lekar.Prezime;*/
                TxtKorisnickoIme.IsEnabled = false;
            }
            else
            {
                this.Title = "Dodaj lekara";
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {

            this.DialogResult = false;
            this.Close();
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            if (!Validation.GetHasError(TxtKorisnickoIme) && !Validation.GetHasError(TxtEmail) && !Validation.GetHasError(TxtName))
            {
                if (odabranStatus.Equals(EStatus.Dodaj))
                {
                    odabranLekar.Aktivan = true;
                    Lekar lekar = new Lekar
                    {
                        DomZdravlja = dzL,
                        Korisnicko = odabranLekar
                    };
                    Util.Instance.Domovi.Add(dzL);
                    Util.Instance.Korisnici.Add(odabranLekar);
                    Util.Instance.Lekari.Add(lekar);

                    int id = Util.Instance.SacuvajK(odabranLekar);//vraca id lekara koji je sacuvan u tabeli-korisnikka koji je sacuva u tabeli
                    lekar.ID = id;
                    Util.Instance.SacuvajK(lekar);

                    int idDz = Util.Instance.SacuvajE(dzL);
                    lekar.DomZdravlja.ID = idDz;
                    Util.Instance.SacuvajE(lekar);


                }

                this.DialogResult = true;
                this.Close();
            }


        }
    }
}
